<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Web statis</title>
</head>

<body>
    <div class="isi">
        <!-- Bagian paling atas -->
        <div>
            <h1>SanberBook</h1>
            <h2>Social Media Developer Santai Berkualitas</h2>
            <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
        </div>

        <!-- Unordered list -->
        <div>
            <h2>Benefit Join di SanberBook</h2>
            <ul>
                <li>Mendapatkan motivasi dari sesama developer </li>
                <li>Sharing knowledge dari para mastah Sanber</li>
                <li>Dibuat oleh calon web developer terbaik</li>
            </ul>
        </div>

        <!-- Ordered list -->
        <div>
            <h2>Cara Bergabung ke SanberBook</h2>
            <ol>
                <li>Mengunjungi Website ini</li>
                <li>mendaftar di <a href="/register">Form Sign Up</a></li>
                <li>Selesai!</li>
            </ol>
        </div>
    </div>
    <img src="" alt="">
</body>

</html>
