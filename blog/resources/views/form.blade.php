<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registrasi</title>
</head>
<body>
            <!-- Heading dan Sub Bab -->
    <div>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
    </div>

    <!-- Form -->
    <div>
        <form action="/welcome" method="POST">
            @csrf
            <!-- input berupa text -->
            <label for="first_name">First Name:</label>
            <br><br>
            <input type="text" name="nama_depan" placeholder="Nama Depan" id="first_name">
            <br><br>
            <label for="last_name">Last Name:</label>
            <br><br>
            <input type="text" name="nama_belakang" placeholder="Nama Belakang" id="last_name">
            <br><br>

            <!-- input berupa radio button -->
            <label for="gender">Gender:</label>
            <br><br>
            <input type="radio" name="gender" value="0">Male
            <br>
            <input type="radio" name="gender" value="1">Female
            <br>
            <input type="radio" name="gender" value="2">Other
            <br><br>

            <!-- input berupa drop down option  -->
            <label>Nationality:</label>
            <br><br>
            <select>
                <option selected disabled hidden>Pilih Negara</option>
                <option value="Indonesia">Indonesia</option>
                <option value="Singapore">Singapore</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Australia">Australia</option>
            </select>
            <br><br>

            <!-- input berupa checkbox -->
            <label>Language Spoken:</label>
            <br><br>
            <input type="checkbox" name="0">Bahasa Indonesia
            <br>
            <input type="checkbox" name="1">English
            <br>
            <input type="checkbox" name="2">Other
            <br><br>

            <!-- input berupa text area -->
            <label for="bio">Bio:</label>
            <br><br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
            <br>

            <!-- tombol submit -->
            <button type="submit">Sign up</button>
        </form>
    </div>
</body>
</html>